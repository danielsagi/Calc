#pragma once
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Helper
{
public:
	Helper();
	~Helper();

	static void trimEnd(string& s);
	static void trimComment(string& s);

	static bool isBackSlashEnding(string& s);
	static vector<int> vectorStoi(vector<string>& base);
};

