#include "AddOperator.h"



AddOperator::AddOperator()
{
}

AddOperator::AddOperator(std::vector<int> values)
{
	this->values = values;
}

AddOperator::~AddOperator()
{
}

double AddOperator::calculate()
{
	double result = 0;
	for (int i = 0; i < this->values.size(); i++) {
		result += this->values[i];
	}
	return result;
}
