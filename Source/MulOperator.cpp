#include "MulOperator.h"



MulOperator::MulOperator()
{
}

MulOperator::MulOperator(std::vector<int> values)
{
	this->values = values;
}


MulOperator::~MulOperator()
{
}

double MulOperator::calculate()
{
	double result = (double)this->values[0];
	for (int i = 1; i < this->values.size(); i++)
		result *= this->values[i];
	return result;
}
