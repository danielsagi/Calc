#pragma once
#include <exception>
#include <string>

using namespace std;

class OperandError : public exception
{
public:
	OperandError();
	~OperandError();

	virtual const char* what() const throw() {
		return "Operand is not valid";
	}
};

