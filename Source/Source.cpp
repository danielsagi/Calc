#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <regex>
#include <map>

#include "Types.h"
#include "Operator.h"
#include "AddOperator.h"
#include "SubOperator.h"
#include "MulOperator.h"
#include "DivOperator.h"

#include "CommandError.h"
#include "DataBase.h"
#include "Helper.h"

std::vector<std::string> sliceInput(std::string input) {
	std::istringstream iss(input);
	std::vector<std::string> sliced;

	for (std::string s; iss >> s;) {
		sliced.push_back(s);
	}

	return sliced;
}

Operator* initOperator(std::vector<std::string> sliced)
{
	Operator* op = nullptr;

	if (sliced.size() > 0)
	{
		Types::OperatorType operatorType = Types::getTypeFromString(sliced[0]);
		sliced.erase(sliced.begin());

		switch (operatorType)
		{
		case Types::add:
			op = new AddOperator(Helper::vectorStoi(sliced));
			break;
		case Types::sub:
			op = new SubOperator(Helper::vectorStoi(sliced));
			break;
		case Types::mul:
			op = new MulOperator(Helper::vectorStoi(sliced));
			break;
		case Types::div:
			op = new DivOperator(Helper::vectorStoi(sliced));
			break;
		case Types::unknown:
			throw CommandError();
			break;
		}
	}

	return op;
}

double runCommand(std::string command) {
	std::vector<std::string> sliced = sliceInput(command);
	
	Operator* currOperator = initOperator(sliced);
	if (currOperator == nullptr)
		throw CommandError();

	double result = currOperator->calculate();
	delete currOperator;
	return result;
}


void substituteCommands(std::string& command) {
	std::smatch matches;
	std::regex pattern("\\$\\(([\\w\\s]+)\\)");

	while (std::regex_search(command, matches, pattern)) {
		double result = runCommand(matches[1].str());

		// updating command with new result
		std::stringstream ss;
		ss << matches.prefix() << result << matches.suffix();
		command = ss.str();
	}
}

bool saveVar(std::string command, DataBase& db) {
	bool saved = false;
	std::smatch matches;
	std::regex pattern("([\\b\\w_]*)=(\\w*)");
	
	while (std::regex_search(command, matches, pattern)) {
		db.insertOrUpdate(matches[1], std::atoi(matches[2].str().c_str()));
		saved = true;
		command = matches.suffix();
	}
	return saved;
}

void substituteVars(std::string& command, DataBase& db) {
	std::regex pattern("\\$(\\b[\\w_]+)");
	std::smatch matches;

	while (std::regex_search(command, matches, pattern)) {
		double value = db.getValue(matches[1]);
		
		// updating command with new results
		std::stringstream ss;
		ss << matches.prefix() << value << matches.suffix();
		command = ss.str();
	}
}

int main(void) {
	std::cout << "Welcome To Calc!\nWritten By - Daniel Sagi\n\n";

	std::string buffer;

	DataBase db;
	while (true) {
		double output = 0;
		std::cout << ">> ";
		std::getline(std::cin, buffer);
		Helper::trimComment(buffer);
		
		// Building continuing command, taking care of "\" and comments 
		while (Helper::isBackSlashEnding(buffer)) {
			std::cout << "...";
			string tempBuff;
			std::getline(std::cin, tempBuff);
			
			Helper::trimComment(buffer);
			Helper::trimEnd(buffer);
			buffer += tempBuff;
		}

		if (buffer.length() <= 0)
			continue;
		
		try {
			substituteVars(buffer, db);
			substituteCommands(buffer);
			if (saveVar(buffer, db))
				continue;
			output = runCommand(buffer);
		}
		catch (exception& e) {
			cout << "Error: " << e.what() << endl;
			continue;
		}

		cout << output << endl;
	}
	
	return 0;
}