#pragma once
#include "Operator.h"

class DivOperator : public Operator
{
public:
	DivOperator();
	DivOperator(std::vector<int> values);
	~DivOperator();

	double calculate();
};

