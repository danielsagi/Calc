#include "SubOperator.h"



SubOperator::SubOperator()
{
}

SubOperator::SubOperator(std::vector<int> values)
{
	this->values = values;
}


SubOperator::~SubOperator()
{
}

double SubOperator::calculate()
{
	double result = (double)this->values[0];
	for (int i = 1; i < this->values.size(); i++)
		result -= this->values[i];
	return result;
}
