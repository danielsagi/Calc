#pragma once
#include <string>
#include <map>

#include "VarNotFoundError.h"

using namespace std;

class DataBase
{
public:
	DataBase();
	~DataBase();

	void insertOrUpdate(string name, double value);
	double getValue(string name);

private:
	map<string, double> underlying;
};

