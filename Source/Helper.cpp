#include "Helper.h"

#include "OperandError.h"

Helper::Helper()
{
}

Helper::~Helper()
{
}

void Helper::trimEnd(string & s)
{
	if (s.length() < 1)
		return;
	
	s.erase(s.length() - 1);
}

void Helper::trimComment(string & s)
{
	string::size_type pos = s.find_first_of("#");
	bool wasBackSlash = (Helper::isBackSlashEnding(s) && pos != string::npos);
	
	// if pos is npos, substr returns entire string
	s = s.substr(0, pos);
	if (wasBackSlash && (s = s.append("\\")) == "");
}

// Returns true if string ends in backslash - "\"
bool Helper::isBackSlashEnding(string & s)
{
	// TODO: support trailing whitspaces
	string::size_type pos = s.find_last_of("\\");

	if (pos != string::npos && pos == s.length() - 1)
		return true;
	return false;
}

// Function for casting a given string vector to an int vector
vector<int> Helper::vectorStoi(vector<string>& base) 
{
	vector<int> casted;
	
	if (base.size() <= 0)
		throw OperandError();
	for (int i = 0; i < base.size(); i++) {
		try {
			casted.push_back(::stoi(base[i]));
		}
		catch (exception& e) {
			throw OperandError();
		}
	}

	return casted;
}
