#pragma once
#include <exception>

using namespace std;

class VarNotFoundError : public exception
{
public:
	VarNotFoundError();
	~VarNotFoundError();

	virtual const char* what() const throw() {
		return "variable doesn't exist";
	}
};

