#pragma once
#include <exception>

class CommandError : public std::exception
{
public:
	CommandError();
	~CommandError();

	virtual const char* what() const throw()
	{
		return "not a recognized command";
	}
};

