#pragma once
#include "Operator.h"

class SubOperator : public Operator
{
public:
	SubOperator();
	SubOperator(std::vector<int> values);
	~SubOperator();

	double calculate();
};

