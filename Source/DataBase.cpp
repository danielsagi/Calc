#include "DataBase.h"



DataBase::DataBase()
{
}


DataBase::~DataBase()
{
}

void DataBase::insertOrUpdate(string name, double value)
{
	this->underlying[name] = value;
}

double DataBase::getValue(string name)
{
	map<string, double>::iterator it = this->underlying.find(name);
	if (it != this->underlying.end())
		return this->underlying[name];
	else
		throw VarNotFoundError();
}
