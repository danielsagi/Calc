# Calc - integers arithmetics

If command does not exists throw CommandError

## Stage 1
Support add operator
If an operant is invalid throw an OperandError
add <op 1> <op 2>

## Stage 1.5
Support multi line commands (line ending with backslash)
Print 3 dots when reading another line
add <op 1> \
...	<op 2>

## Stage 2
Support comments
add <op 1> <op 2> # Comment

## Stage 3
Support sub operator
sub <op1> <op2>

## Stage 4
Extend commands to retrieve variable arguments (will act as reduce), single argument is valid
add <op 1> <op 2> <op 3> ... <op N>

## Stage 5
Provide command substitution feature
Command substitution can be multiline without backslash)
add <op 1> $(sub <op 1> <op 2>)
sub <op 1> $(add <op 1>
...	<op 2>)

## Stage 6
Provide a way to store results in variables
Typing variable name with $ preceding, will print its value
my_var = $(add <op 1> <op 2>)

## Stage 7
Support variables to be operands
add $var_a $var_b

## Stage 8
Support shortened arithmetics
var_a += <op 1> <op 2> ... <op N> # Equals var_a = $(add $var_a <op 1> <op 2> ... <op N>)

## Stage 9
Support division and multiplication
mul <op 1> <op 2>
div <op 1> <op 2>
