#pragma once
#include "Operator.h"

class MulOperator : public Operator
{
public:
	MulOperator();
	MulOperator(std::vector<int> values);
	~MulOperator();

	double calculate();
};

