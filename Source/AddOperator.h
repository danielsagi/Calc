#pragma once
#include "Operator.h"

class AddOperator : public Operator
{
public:
	AddOperator();
	AddOperator(std::vector<int> values);

	~AddOperator();

	double calculate();
};

