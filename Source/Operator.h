#pragma once
#include <string>
#include <vector>
#include <stdio.h>

#include "OperandError.h"

class Operator 
{
public:
	Operator();
	virtual ~Operator();

	virtual double calculate() = 0;

protected:
	std::vector<int> values;
};