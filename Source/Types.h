#pragma once
#include <string>

namespace Types {
	enum OperatorType
	{
		add,
		sub,
		mul,
		div,
		unknown,
	};

	OperatorType getTypeFromString(std::string op)
	{
		OperatorType ret = unknown;
		if (op == "add")
			ret = add;
		else if (op == "sub")
			ret = sub;
		else if (op == "mul")
			ret = mul;
		else if (op == "div")
			ret = div;
		return ret;
	}
}