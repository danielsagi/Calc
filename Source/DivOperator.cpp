#include "DivOperator.h"



DivOperator::DivOperator()
{
}

DivOperator::DivOperator(std::vector<int> values)
{
	this->values = values;
}


DivOperator::~DivOperator()
{
}

double DivOperator::calculate()
{
	double result = this->values[0];
	for (int i = 1; i < this->values.size(); i++) {
		result /= this->values[i];
	}
	return result;
}